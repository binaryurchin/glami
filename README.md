# Create DB containers

## Install control node

```shell
mkdir ansible
cd ansible
python -m venv .venv
source .venv/bin/activate
pip3 install ansible
ansible-galaxy collection install community.docker
git clone git@gitlab.com:binaryurchin/glami.git .
```

## Install docker on managed nodes

```shell
ansible-playbook playbooks/install_docker.yml
```

## Create stack for one location

```shell
ansible-playbook playbooks/create_containers.yml --extra-vars "geo=cz";
```

## Create stack on mulitple locations

```shell
for geo in cz sk pl; do
ansible-playbook playbooks/create_containers.yml --extra-vars "geo=$geo";
done
```
